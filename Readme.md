# Prosumer Markets Simulator

This open-source platform has been developed to facilitate the design and simulation of decentralized electricity markets. It has been developed in Python with Dash library (Plotly) to form of a web based app. The platform is currently composed of two modules. The simulation module allows to design and simulate test cases, while the generator module creates synthetic test cases.

# Wiki

Platform's wiki is available [here](https://gitlab.com/fmoret/P2PApp/wikis/home).

# App setup

To setup the app launch the "create_update_environment.bat" file.
(Note that you might need to launch it via the python terminal.)

At the request
```sh
Insert gurobi path ...
```
you should answer
```sh
Insert=$YourGurobiPath
```
For example:
```sh
Insert=C:\gurobi752\win64\
```

# App start-up

You only need to launch the "start.bat" file.

The terminal should show the following reply of Dash's local server:
```sh
 * Restarting with stat
 * Debugger is active!
 * Debugger PIN: 984-449-674
 * Running on http://127.0.0.1:8050/ (Press CTRL+C to quit)
```
- You can now reload your web browser tab, openned at the given URL address http://127.0.0.1:8050 in this example


# Proposed improvements

The following points will/need to be developed:
- Market tab:
    - Improvement of graph plot
    - Interaction between graph plot and side menu
- Simulation tab:
    - Implement other simulation types
    - Save simulation parameters
    - Save results
    - Deployment on a High Power Computing (HPC) such as on Amazon Web Services (optional)
    - Report generation: 
        - Add other relevant results
        - generate a PDF report (optional)

